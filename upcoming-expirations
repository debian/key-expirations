#!/usr/bin/python3

# Copyright 2020 Felix Lechner <felix.lechner@lease-up.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import datetime
import gnupg
import logging
import re
import os
import smtplib
import sys
import tempfile

from email.message import EmailMessage

SEND=True
DONT_SEND=False

def send_reminder(recipient_mailbox, recipient_name, status, expiration, keyid, sender_name, sender_email, really_send):

    body = '''\
Hi {name},


This is an unofficial reminder that your Debian key {status} on {date}.
Debian does not synchronize its keyring with other public key servers,
so you need to upload your key manually after extending the expiration date.
(There are some instructions on https://keyring.debian.org/)

Your key ID:     {keyid}

It's good to remember that the Debian keyring is updated only once a month,
usually that happens on the 24th of each month.
It is just a few days away.


Regards from the key expiry reminder bot -
who is not really a bot, but don't tell him :)

Also, I'm happy to exclude you from these mails anytime, just tell.
'''.format(name=recipient_name, status=status, date=expiration, keyid=keyid, sender=sender_name)

    if re.search(',', sender_name):
        sender_name = '"{name}"'.format(name=sender_name)

    sender_mailbox = '{name} <{email}>'.format(name=sender_name, email=sender_email)

    message = EmailMessage()
    message["Subject"] = 'Your Debian key {status}'.format(status=status)
    message['To'] = recipient_mailbox
    message['From'] = sender_mailbox
    message['Bcc'] = sender_mailbox

    message.set_content(body)

    if really_send:
        s = smtplib.SMTP('localhost')
        s.send_message(message)
        s.quit()

        print()
        print('Reminder sent to %s at %s.' % (recipient_name, recipient_mailbox))
        print('A blind copy went to %s.' % sender_mailbox)
    else:
        print()
        print(f"Not sending reminder, mail see below.")
        print(f"{message.as_bytes().decode(encoding='UTF-8')}")
        print("---------------------")

    return

parser = argparse.ArgumentParser(description='Send reminders for expiring Debian keys.')
parser.add_argument('--sender-name', metavar='NAME', type=str, required=True, help='Full name of sender and signatory')
parser.add_argument('--sender-email', metavar='EMAIL', type=str, required=True, help='Bare email address of sender')
args = parser.parse_args()

sender_name = args.sender_name
sender_email = args.sender_email

now = datetime.datetime.utcnow()
print("Today's date: " + now.strftime('%Y-%m-%d'))

keyring = 'debian-keyring.gpg'

homedir = tempfile.TemporaryDirectory()
gpg = gnupg.GPG(gnupghome=homedir.name)
gpg.encoding = 'utf-8'

with tempfile.TemporaryDirectory() as downloaddir:
    os.system('rsync -az --progress keyring.debian.org::keyrings/keyrings/%s %s' %(keyring, downloaddir));
    keys = gpg.scan_keys(downloaddir + '/' + keyring)

print('Checking %d keys from keyring %s.' % (len(keys), keyring))

expires = [key for key in keys if key["expires"]]

for key in expires:
    key['days left'] = int((datetime.datetime.fromtimestamp( float(key['expires']) ) - now).days)

by_date = sorted(expires, key = lambda x: x['days left'])
recent = [key for key in by_date if key['days left'] > -31]

alert_days = 40
need_attention = [key for key in recent if key['days left'] < alert_days]

for keyring_key in need_attention:

    print()

    keyring_date = datetime.datetime.utcfromtimestamp(float(keyring_key['expires'])).strftime('%Y-%m-%d')
    keyring_days = keyring_key['days left']

    status = None
    if keyring_days <= 0:
        status = 'expired'
        print('Expired %d days ago (on %s):' % (-keyring_days, keyring_date))
    else:
        status = 'is expiring'
        print('Expiring in %d days (on %s):' % (keyring_days, keyring_date))

    print('First uid in keyring is %s' % (keyring_key['uids'][0]))

    keyid = keyring_key['keyid']

    from_server = gpg.recv_keys('keyring.debian.org', keyid)
    if from_server.count != 1:
        print('Key not found on key server.')
        continue

    updated = gpg.list_keys(keys=keyid)
    if len(updated) != 1:
        print('Did not find exactly one key on key server.')
        continue

    server_key = updated[0]

    if server_key['expires'] == '':
        continue

    server_date = datetime.datetime.utcfromtimestamp(float(server_key['expires'])).strftime('%Y-%m-%d')
    server_key['days left'] = int((datetime.datetime.fromtimestamp( float(server_key['expires']) ) - now).days)
    server_days = server_key['days left']

    if server_days > keyring_days:
        print('Update found on key server.')
        print('Now expires on %s.' % (server_date))
        continue

    position = 1
    for uid in server_key['uids']:
          print('  uid %d: %s' % (position, uid))
          position += 1

    action = input('Send a reminder (pick a uid)? (#/n/q): ')
    if action == 'q':
        sys.exit()
    if not re.match('^\d+$', action):
        continue

    selected_position = int(action)
    recipient_mailbox = server_key['uids'][selected_position - 1]
    if re.match('^"?([^ <>]+)', recipient_mailbox) != None:
        first_name = re.match('^"?([^ <>]+)', recipient_mailbox).group(1) or recipient_mailbox
    else:
        print("got None\n")
        first_name = recipient_mailbox

    recipient_name = input('Name used as greeting in letter [%s]: ' % first_name) or first_name

    send_reminder(recipient_mailbox, recipient_name, status, server_date, keyid, sender_name, sender_email, DONT_SEND)
    print(f"bla {recipient_name} {status} {keyid}")

sys.exit()
