Don't send out reminders without coordination,
otherwise people could receive several "your key is expiring" mails.

This is now in the final transformation from manual sending towards a cron job,
that runs every month on the 17th \o/

Obviously the cron job uses noninteractive-upcoming-expirations,
not the formerly used script upcoming-expirations.
Also things like dates are hardcoded right now, it's still in the test phase.
It shouldn't run amok and spam everyone at least.

Outdated
======================================

Right now this is done as simply as it can be -
Once per month, about 7 to 10 days before the next keyring update
upcoming-expirations is called to collect data and print out mails,
then mails are sent manually to all who would be affected by their key expiration.
Meaning, that their keys would expire before the update after the next update.
The script is not yet updated to reflect that,
just was altered to no longer send mails itself - blame laziness.

[This project has changed maintainer and was forked not so long ago.
The README below is partly outdated]

Reminders for Upcoming Key Expirations
======================================

People and their contributions are Debian's greatest asset. As a
project, it makes no sense to keep people from maintaining packages,
yet that is exactly what happens when we honor the expiration dates on
their GPG keys.

The issue is exacerbated by the fact that we do not synchronize our
keyring with other, perhaps more commonly used key servers.

This script downloads a copy of the Debian keyring to a temporary
location, analyzes the dates without importing anything into your
personal keyring, and offers to send messages interactively.

You will be asked to select a single `uid` as the recipient and
receive a blind copy of the reminder that was sent.

Sample Command
==============

Please replace the fields below with your information:
```
./upcoming-expirations --sender-name 'A Friend' --sender-email 'a.friend@receives.bcc'
```

To Do
==================

Some people use master keys that do not expire but set expiration
dates on subkeys. It would be great to catch those also.

Signing subkeys would be the most important, although it has been
pointed out that valid encryption subkeys are required to receive
messages from `changes2db.debian.org`.

There is a little bit of work involved in figuring out which subkeys
are superseeded and will therefore not be renewed. Those do not need
any reminders.

- I would assume to simply ignore subkeys that
already have exipred longer than a month or so.

Trickier and with no good solution - there are rumors about people
that use gpg for other things than Debian,
so not all their subkeys might be relevant for Debian things.


Another thing - currently deciding to who to send a reminder is purely manual.
I attempt to send one to all who will be actually affected by missing the next keyring update.
Though some expiration dates like 23rd to 24th are kind of corner case
and depending on my mood I decide the one or other way round.
It would be better to let the script decide such things, so they are more consistent.

Also - should the name of the person contain any non-latin char, the script
spits out a base64 encoded mail, what is annoying.

I don't attempt to fix automatic sending, as there is some state,
that is hard to codify (eg, I don't send a second mail to so
who already got a mail last month)
